#!/bin/bash
# This script installs [gomatic](https://github.com/gocd-contrib/gomatic/) onto
# the server so we can modify the initial config file e.g. set the agent secret.
#
# This normally happens during the initial setup by the user.
# After that the config file is fully managed by GoCD.
#
# EXAMPLE
#
# Call the script like this after the server is completely started:
#
#    docker-compose exec gocd-server python /tmp/config/setup.py
#
# BACKGROUND
#
# The GoCD image does not respect the 'AGENT_AUTO_REGISTER_KEY' key
# anymore so we handle the whole XML file like this.
# We do not mount the file directly because GoCD will modify the contents which
# leads to modification and we want to protect the user from these git changes.
# We also ran into several issues when installing python packages on the host so
# we 'bundle' it with the server container.
set -eu

if python -c "import gomatic" &> /dev/null; then
  echo "Gomatic is already installed. Exiting..."
  exit 0
fi

apk add --update \
    python \
    python-dev \
    py-pip

pip install gomatic
