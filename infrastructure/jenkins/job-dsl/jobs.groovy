def gitRootUrl = 'git://github.com/your-org/'

pipelines = [
  [
    name: 'project1',
    repository: 'project1',
    description: 'Web App'
  ],
  [
    name: 'project2',
    repository: 'project2',
    description: 'API server'
  ]
].each { project ->
  job(project['name']) {
    scm {
      git(gitRootUrl + project['repository'])
    }

    triggers {
      scm('*/15 * * * *')
    }

    steps {
      shell('bash -e scripts/pipeline/debug.sh')
    }
  }
}
