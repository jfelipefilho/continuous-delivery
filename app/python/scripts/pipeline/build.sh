#!/bin/bash

echo "Setup project structure"
mkdir -p bin/
mkdir -p test/
mkdir -p reports/

echo "Building..."
sleep 2
touch bin/static.app

echo "Running tests..."
sleep 1
echo "Test Report" > reports/index.html
